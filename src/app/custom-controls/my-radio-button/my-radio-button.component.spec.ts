import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRadioButtonComponent } from './my-radio-button.component';

describe('MyRadioButtonComponent', () => {
  let component: MyRadioButtonComponent;
  let fixture: ComponentFixture<MyRadioButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyRadioButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyRadioButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
