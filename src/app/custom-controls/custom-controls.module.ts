import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {customComponents} from './index';

@NgModule({
  declarations: [customComponents],
  imports: [
    CommonModule
  ],
  exports: [customComponents],
})
export class CustomControlsModule { }
