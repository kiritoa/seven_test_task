import { MyRadioButtonComponent } from './my-radio-button/my-radio-button.component';
import { MyInputComponent } from './my-input/my-input.component';

export const customComponents = [
  MyRadioButtonComponent,
  MyInputComponent,
];
